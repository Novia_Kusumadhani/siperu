-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Jul 2022 pada 09.58
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemruang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kd_barang` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id`, `kd_barang`, `nm_barang`, `created_at`, `updated_at`) VALUES
(1, 'B001', 'Meja Mimbar', '2021-12-13 03:42:24', '2021-12-13 22:41:39'),
(2, 'B002', 'Sound System', '2021-12-13 03:42:42', '2021-12-13 03:42:42'),
(3, 'B003', 'Microfon', '2021-12-13 03:42:58', '2021-12-13 03:43:06'),
(4, 'B004', 'Kursi', '2021-12-15 05:27:07', '2021-12-15 05:27:07'),
(5, 'B005', 'LCD Projektor', '2021-12-15 05:27:22', '2021-12-15 05:27:22'),
(6, 'B006', 'Karpet', '2021-12-15 05:27:44', '2021-12-15 05:27:44'),
(7, 'B007', 'Meja Besar', '2021-12-15 05:28:00', '2021-12-15 05:28:00'),
(8, 'B008', 'Layar LCD', '2021-12-15 05:29:47', '2021-12-15 05:29:57'),
(9, 'B009', 'Pena', '2021-12-15 05:30:09', '2021-12-15 05:30:09'),
(10, 'B010', 'Buku', '2021-12-15 05:30:47', '2021-12-15 05:30:47'),
(11, 'B011', 'Solasi', '2021-12-15 05:32:39', '2021-12-15 05:32:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `waktu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id`, `waktu`, `created_at`, `updated_at`) VALUES
(1, '08:00 - 09:00', '2021-12-11 07:22:32', '2021-12-11 07:22:49'),
(2, '09:00 - 10:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(3, '10:00 - 11:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(4, '11:00 - 12:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(5, '12:00 - 13:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(6, '13:00 - 14:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(7, '14:00 - 15:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(8, '15:00 - 16:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(9, '17:00 - 18:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(10, '18:00 - 19:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(11, '19:00 - 20:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49'),
(12, '20:00 - 21:00', '2021-12-11 07:22:49', '2021-12-11 07:22:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2014_10_12_000000_create_users_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(51, '2019_08_19_000000_create_failed_jobs_table', 1),
(52, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(53, '2021_12_07_133915_create_barang', 1),
(54, '2021_12_07_134118_create_peminjaman', 1),
(55, '2021_12_07_134143_create_ruang', 1),
(56, '2021_12_10_185348_create_jadwal', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('Mahasiswa','Pegawai') COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_induk` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nohp` char(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `barang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `jadwal_id` bigint(20) UNSIGNED DEFAULT NULL,
  `kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `sts_pinjam` int(11) DEFAULT NULL,
  `peserta` int(11) NOT NULL,
  `path_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namefile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `name`, `level`, `no_induk`, `prodi`, `nohp`, `ruang_id`, `barang_id`, `jadwal_id`, `kegiatan`, `tgl_kegiatan`, `sts_pinjam`, `peserta`, `path_file`, `namefile`, `created_at`, `updated_at`) VALUES
(1, 'Mahasiswa', 'Mahasiswa', 'M3118004', 'D3 Teknik Informatika', '081770572435', 1, 1, 4, 'Perkuliahan', '2021-12-13', 1, 12, 'SIK/1637550918686.jpg', '1637550918686.jpg', '2021-12-13 05:46:40', '2021-12-13 05:48:35'),
(7, 'Bara', 'Mahasiswa', 'M3118004', 'D3 Teknik Informatika', '081234567890', 1, 3, 3, 'Pleno', '2021-12-16', 2, 20, 'SIK/SOAL PRE dan POST Test Farmasi_171020 (2).pdf', 'SOAL PRE dan POST Test Farmasi_171020 (2).pdf', '2021-12-15 05:25:26', '2021-12-15 05:26:29'),
(8, 'Baskara', 'Mahasiswa', 'M3118005', 'D3 Teknik Informatika', '086523971895', 2, 9, 11, 'Rapat', '2022-01-20', NULL, 21, 'SIK/CamScanner 09-25-2021 20.09.pdf', 'CamScanner 09-25-2021 20.09.pdf', '2022-01-16 21:29:17', '2022-01-16 21:29:17'),
(11, 'Bara', 'Mahasiswa', 'M3118004', 'D3 Teknik Informatika', '083486913457', 3, 8, 3, 'Rapat', '2022-01-25', NULL, 22, 'SIK/CETAK DATA RUANG.pdf', 'CETAK DATA RUANG.pdf', '2022-01-16 23:26:08', '2022-01-16 23:26:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kd_ruang` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `fasilitas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id`, `kd_ruang`, `nm_ruang`, `kapasitas`, `fasilitas`, `created_at`, `updated_at`) VALUES
(1, 'R001', 'Ruang Rapat 1', 60, '60 Kursi, 2 Meja Besar, LCD', '2021-12-13 03:49:59', '2021-12-13 03:49:59'),
(2, 'R002', 'Ruang Rapat 2', 50, '50 Kursi, 1 Mimbar, 2 Sound System', '2021-12-13 03:50:41', '2021-12-13 03:50:41'),
(3, 'R003', 'Ruang Rapat 3', 30, '30 Kursi, 2 Meja Besar, 2 AC', '2021-12-13 03:51:25', '2021-12-13 03:51:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `verify` tinyint(11) DEFAULT 1,
  `nim` char(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Mahasiswa','Pegawai') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` enum('Admin','User','Moderator') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `nomorhp` char(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `verify`, `nim`, `tahun`, `prodi`, `name`, `status`, `level`, `email`, `email_verified_at`, `nomorhp`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 'Admin', NULL, 'Admin', 'admin@gmail.com', NULL, NULL, '$2y$10$Q78wJ23t2NG1p0.zLHsoe.4Wgbj5Y1YONX7ZcUE66cm5rxmAzzSAS', NULL, '2021-12-13 08:16:29', '2021-12-13 08:16:29'),
(2, 1, 'M3118002', '2018', 'D3 Teknik Informatika', 'Mahasiswa', 'Mahasiswa', 'User', 'mahasiswa@gmail.com', NULL, '087664129646', '$2y$10$LZJ5pHQcTcbCZch6li8gC.B23c4ko3nvKqbJpmQAUk5p.RLvyM2pe', NULL, '2021-12-13 05:37:53', '2021-12-13 05:38:52'),
(3, 0, 'M3118003', '2018', 'D3 Teknik Informatika', 'Mahasiswa 2', 'Mahasiswa', 'User', 'mahasiswa2@gmail.com', NULL, '081212121212', '$2y$10$fj7BkB.P3MxFbtFW6C/z8uWbvNfkVxPT0anoqWQHu4EfEQJ3n.A6O', NULL, '2021-12-13 05:49:26', '2022-01-16 21:08:57'),
(5, 1, 'M3118004', '2018', 'D3 Teknik Informatika', 'Bara', '', 'User', 'bara@student.uns.ac.id', NULL, '081234567890', '$2y$10$E6VsCe66ciEMr7mxWvl55uAAqqOUyrwa68DWckD682GpWdy0xl4qW', NULL, '2021-12-13 23:14:46', '2021-12-15 05:33:54'),
(6, 1, 'M3118005', '2018', 'D3 Teknik Informatika', 'Baskara', 'Mahasiswa', 'User', 'baskara@student.uns.ac.id', NULL, '087664129649', '$2y$10$vhvoAHP7snIDRkuTWlbdlONmQ03OBsZPCMuju/JQFYdxz3638EEhO', NULL, '2022-01-16 21:24:27', '2022-01-16 21:24:27');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barang`
--
ALTER TABLE `barang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
